import React ,{useRef} from "react";
import { AiFillEdit} from "react-icons/ai";
import {IoCheckmarkDoneSharp,IoClose} from "react-icons/io5";
const TodoItem = (props) =>{
    const { item ,updateTodo,deleteTodo,completeTodo } = props;
    const inputRef = useRef(true);
    const changeFocus = () =>{  
        inputRef.current.disabled = false;
        inputRef.current.focus();
    }

    const update = (id,value,e) => {
        if(e.which === 13){
            //13 is the key code of enterkey
            updateTodo({id,item: value});
            inputRef.current.disabled = true;
        }
    };
    return(
        <li className="card" key={item.id}>

            <textarea ref={inputRef} disabled={inputRef} defaultValue={item.item} 
                onKeyPress={(e)=>update(item.id,inputRef.current.value,e)}/>
                
            <div className="btns">
                <button 
                onClick={() => changeFocus()}>
                <AiFillEdit/> 
                </button>
                {item.completed === false && (
                    <button style={{ color: "green"}} 
                        onClick={() => completeTodo(item.id)}>
                        <IoCheckmarkDoneSharp/>
                    </button>
                )}

                <button style={{ color: "red"}} 
                    onClick={() => deleteTodo(item.id)}>
                   <IoClose/>
                </button>
            </div>
            {item.completed && <span className="completed">Done</span>}
        </li>
    );
}
export default TodoItem;
/*
useRef returns a mutable ref object
 whose .current property is initialized to the passed argument (initialValue)
 */
import { createSlice } from "@reduxjs/toolkit";
const initialState = [];
const addTodoReducer = createSlice({
    name: "Todos",
    initialState,
    reducers:{
        //add todo
        addTodos:(state,action) =>{
            state.push(action.payload);
            return state;
        },
        //update todo
        updateTodo: (state,action) =>{
            return state.map((todo)=>{
                if(todo.id === action.payload.id){
                    return{
                        ...todo,
                        item: action.payload.item,
                    };
                }  
                return todo;
            });   
        },
        //delete todo
        deleteTodo:(state,action) =>{
            return state.filter((item)=>item.id !== action.payload);
        },
        //completed item 
        completeTodo:(state,action)=>{
            return state.map((todo)=>{
                if(todo.id === action.payload){
                    return{
                        ...todo,
                        completed: true,
                    };
                }  
                return todo;
            });   
        },
    },
})
export const { addTodos ,updateTodo, deleteTodo,completeTodo } = addTodoReducer.actions;
export const reducers = addTodoReducer.reducer;